#include<iostream>

bool isEqual(int a, int b)
{
	return (a == b);
}

bool isNotEqual(int a, int b)
{
	return (a != b);
}

int main()
{
	int value1{ 0 };
	std::cout << "Enter the First Value: ";
	std::cin >> value1;
	std::cout << '\n';

	int value2{ 0 };
	std::cout << "Enter the Second Value:";
	std::cin >> value2;
	std::cout << '\n';

	std::cout << std::boolalpha;

	std::cout << value1 << " and " << value2 << " are equal? ";
	std::cout << isEqual(value1, value2) << '\n';

	std::cout << value1 << " and " << value2 << " are not equal? ";
	std::cout << isNotEqual(value1, value2) << '\n';
	
	return 0;
}